from django.utils.translation import gettext_lazy as _

from electeez_common.settings import *

#from dotenv import load_dotenv
#load_dotenv()

BASE_DIR = Path(__file__).resolve().parent

SITE_ID = 1
SITE_NAME = 'test'

INSTALLED_APPS += ['tests']

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('fr', _('French')),
    ('en', _('English'))
)

TIME_ZONE = 'UTC'
USE_TZ = True

USE_I18N = True
USE_L10N = True

STATIC_URL = '/static/'
STATIC_ROOT_DIR = 'public'
STATIC_ROOT = BASE_DIR / STATIC_ROOT_DIR
STATICFILES_DIRS = [
    BASE_DIR / 'static',
]

if DEBUG:
    STATIC_URL = '/static/'
    STATIC_ROOT_DIR = 'public/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.getenv('MEDIA_ROOT', BASE_DIR / 'media')

DATABASES = {
    'default': {
        'ENGINE': os.getenv('DB_ENGINE', 'django.db.backends.postgresql'),
        'NAME': os.getenv('DB_NAME', 'electeez-test'),
        'PASSWORD': os.getenv('DB_PASS', None),
        'HOST': os.getenv('DB_HOST', None),
        'USER': os.getenv('DB_USER', None),
        'PORT': os.getenv('DB_PORT', None),
    }
}

CELERY_BROKER_URL = REDIS_SERVER
CELERY_RESULT_BACKEND = REDIS_SERVER
CELERY_WORKER_REDIRECT_STDOUTS = False
CELERY_TASK_ALWAYS_EAGER = True

DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL', 'noreply@electis.io')
