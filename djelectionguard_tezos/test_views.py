import pytest

from django.core.management import call_command
from django.utils import timezone
from djtezos.models import Blockchain, Account
from electeez_auth.models import User
from electeez_sites.models import Site
from djelectionguard.models import Contest


@pytest.fixture(autouse=True)
def django_fixtures_setup(django_db_setup, django_db_blocker):
    fixture = 'tests/site_data.json'
    with django_db_blocker.unblock():
        call_command('loaddata', fixture)
        Site.objects.clear_cache()
        Site.objects.get_current()


@pytest.mark.django_db
def test_create_contract(client):
    user = User.objects.create_user(email='admin@example.com')
    client.force_login(user)
    blockchain = Blockchain.objects.create(
        name='tzlocal',
        provider_class='djtezos.tezos.Provider',
        confirmation_blocks=1,
        is_active=True,
        endpoint='http://tz:8732',
    )
    election = Contest.objects.create(
        mediator=user,
        start=timezone.now(),
        end=timezone.now(),
    )

    assert client.get(f'/en/tezos/{election.pk}/create/').status_code == 200

    account = Account.objects.first()
    account.balance = 10.0
    account.save()

    response = client.post(
        f'/en/tezos/{election.pk}/create/',
        data=dict(blockchain=str(blockchain.pk)),
    )
    assert response.status_code == 302
    assert response['Location'] == f'/en/contest/{election.pk}/'
