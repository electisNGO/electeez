import smartpy as sp

class Election(sp.Contract):
  def __init__(self, admin, asset, token_id):
    self.init(
        admin=admin,
        open='',
        close='',
        manifest_hash='',
        manifest_url='',
        artifacts_hash='',
        artifacts_url='',
        scores={},
        winners=sp.set(t=sp.TAddress),
        signatures={},
        asset=asset,
        token_id=token_id
    )

  @sp.entry_point
  def open(self, date, manifest_hash, manifest_url, candidates):
      sp.verify(sp.sender==self.data.admin)
      sp.verify(self.data.open=='')
      self.data.open = date
      self.data.manifest_hash = manifest_hash
      self.data.manifest_url = manifest_url
      sp.for candidate in candidates:
        self.data.scores[candidate] = 0

  @sp.entry_point
  def close(self, date):
      sp.verify(sp.sender==self.data.admin)
      sp.verify(self.data.close=='')
      self.data.close = date

  @sp.entry_point
  def artifacts(self, artifacts_hash, artifacts_url, scores, num_winners):
      sp.verify(sp.sender==self.data.admin)
      sp.verify(self.data.artifacts_hash=='')
      sp.verify(num_winners < sp.len(self.data.scores))
      self.data.artifacts_hash = artifacts_hash
      self.data.artifacts_url = artifacts_url
      sp.for score in scores.items():
        sp.if self.data.scores.contains(score.key):
            self.data.scores[score.key] = score.value

      sp.for i in sp.range(0, num_winners):
        best_key = sp.local('best_key', sp.none)
        best_val = sp.local('best_val', 0)
        sp.for score in self.data.scores.items():
          sp.if ~ self.data.winners.contains(score.key):
            sp.if score.value >= best_val.value:
              best_key.value = sp.some(score.key)
              best_val.value = score.value
        sp.if ~ (best_key.value == sp.none):
          self.data.winners.add(best_key.value.open_some())
          sp.if i == 0:
              self.fa2_transfer(
                  self.data.asset,
                  self.data.admin,
                  best_key.value.open_some(),
                  self.data.token_id,
                  1
              )

  def fa2_transfer(self, fa2, from_, to_, token_id, amount):
    c = sp.contract(sp.TList(sp.TRecord(from_=sp.TAddress, txs=sp.TList(sp.TRecord(amount=sp.TNat, to_=sp.TAddress, token_id=sp.TNat).layout(("to_", ("token_id", "amount")))))), fa2, entry_point='transfer').open_some()
    sp.transfer(sp.list([sp.record(from_=from_, txs=sp.list([sp.record(amount=amount, to_=to_, token_id=token_id)]))]), sp.mutez(0), c)

  @sp.entry_point
  def sign(self):
    sp.verify(self.data.winners.contains(sp.sender))
    self.data.signatures[sp.sender] = 'Approved'


@sp.add_test(name = "set_scores")
def test():
  scenario = sp.test_scenario()
  scenario.h1("Set scores")
  contract = Election(
      sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'),
      sp.address('KT1REThCJisRqNmTi4tgVosNZkBmA478cyue'),
      0
  )
  scenario += contract
  scenario += contract.open(
    date='datestr',
    manifest_hash='efwegwdfqfewe',
    manifest_url='https://manifest.url',
    candidates=[
        sp.address('tz1W1en9UpMCH4ZJL8wQCh8JDKCZARyVx2co'),
        sp.address('tz1UrjyVZueYdd1C79cmGaazgGb7We6YtHav'),
        sp.address('tz1a4Mhd3sESbmjGzE8R48i7nCFsVEkTMZmv')
    ]).run(sender=sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  scenario += contract.close('dateclose').run(sender=sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  scenario += contract.artifacts(
    artifacts_hash='artifacts_hash',
    artifacts_url='https://artifacts_url.com',
    scores=sp.map({
        sp.address('tz1W1en9UpMCH4ZJL8wQCh8JDKCZARyVx2co'): 0,
        sp.address('tz1UrjyVZueYdd1C79cmGaazgGb7We6YtHav'): 1,
        sp.address('tz1a4Mhd3sESbmjGzE8R48i7nCFsVEkTMZmv'): 1
    }),
    num_winners=2
  ).run(sender=sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  scenario += contract.sign().run(sender=sp.address('tz1UrjyVZueYdd1C79cmGaazgGb7We6YtHav'))
  scenario += contract.sign().run(sender=sp.address('tz1a4Mhd3sESbmjGzE8R48i7nCFsVEkTMZmv'))
  sp.add_compilation_target('election', contract, storage = None)

