from django.conf import settings
from django.utils import timezone


contract_file = getattr(
    settings, 'ELECTIONCONTRACT_FILE', 'election_compiled.json'
)

if contract_file == 'election_compiled.json':
    class ContractInterface:
        def open(self):
            return self.call(
                sender=self.sender,
                function='open',
                args=[
                    str(timezone.now()),
                    self.election.manifest_sha512,
                    self.election.manifest_url,
                ],
                state='deploy',
            )

        def close(self):
            return self.call(
                sender=self.sender,
                function='close',
                args=[str(timezone.now())],
                state='deploy',
            )

        def artifacts(self):
            return self.call(
                sender=self.sender,
                function='artifacts',
                args=[
                    self.election.artifacts_sha512,
                    self.election.artifacts_url,
                ],
                state='deploy',
            )

elif contract_file == 'election_score_compiled.json':
    class ContractInterface:
        def open(self):
            return self.call(
                sender=self.sender,
                function='open',
                args=[
                    list(
                        self.election.candidate_set.values_list(
                            'name',
                            flat=True
                        )
                    ),
                    str(timezone.now()),
                    self.election.manifest_sha512,
                    self.election.manifest_url,
                ],
                state='deploy',
            )

        def close(self):
            return self.call(
                sender=self.sender,
                function='close',
                args=[str(timezone.now())],
                state='deploy',
            )

        def artifacts(self):
            return self.call(
                sender=self.sender,
                function='artifacts',
                args=[
                    self.election.artifacts_sha512,
                    self.election.artifacts_url,
                    {c.name: c.score
                        for c in self.election.candidate_set.all()}
                ],
                state='deploy',
            )

elif contract_file == 'election_full_compiled.json':
    class ContractInterface:
        def open(self):
            return self.call(
                sender=self.sender,
                function='open',
                args=[
                    list(
                        self.election.candidate_set.values_list(
                            'name',
                            flat=True
                        )
                    ),
                    str(timezone.now()),
                    self.election.manifest_sha512,
                    self.election.manifest_url,
                ],
                state='deploy',
            )

        def close(self):
            return self.call(
                sender=self.sender,
                function='close',
                args=[str(timezone.now())],
                state='deploy',
            )

        def artifacts(self):
            return self.call(
                sender=self.sender,
                function='artifacts',
                args=[
                    self.election.artifacts_sha512,
                    self.election.artifacts_url,
                    2,
                    {c.name: c.score
                        for c in self.election.candidate_set.all()},
                ],
                state='deploy',
            )

elif contract_file == 'election_nft_compiled.json':
    class ContractInterface:
        def open(self):
            return self.call(
                sender=self.sender,
                function='open',
                args=[
                    list(
                        self.election.candidate_set.values_list(
                            'name',
                            flat=True
                        )
                    ),
                    str(timezone.now()),
                    self.election.manifest_sha512,
                    self.election.manifest_url,
                ],
                state='deploy',
            )

        def close(self):
            return self.call(
                sender=self.sender,
                function='close',
                args=[str(timezone.now())],
                state='deploy',
            )

        def artifacts(self):
            return self.call(
                sender=self.sender,
                function='artifacts',
                args=[
                    self.election.artifacts_sha512,
                    self.election.artifacts_url,
                    1,
                    {c.name: c.score
                        for c in self.election.candidate_set.all()},
                ],
                state='deploy',
            )
