from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Run multiple 0 args commands'

    def add_arguments(self, parser):
        parser.add_argument('run', nargs='+', type=str)

    def handle(self, *args, **options):
        if 'run' in options:
            for command in options['run']:
                if command:
                    call_command(*command.split(' '))
