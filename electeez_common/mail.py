import os
import importlib
import html

from email.mime.image import MIMEImage

from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from djlang.utils import gettext as _
from django.utils.html import strip_tags

from ryzom.html import Markdown, Html, Body

from electeez_sites.models import Site

from django.contrib.auth import get_user_model


if settings.CUSTOM_SEND_MAIL_MOD:
    send_mail_mod = importlib.import_module(settings.CUSTOM_SEND_MAIL_MOD)
    send_mail = send_mail_mod.send_mail

else:
    def attach_image(email, image):
        subtype = None
        if os.path.splitext(image.name)[1] == '.svg':
            subtype = 'svg+xml'

        to_attach = MIMEImage(image.file.read(), _subtype=subtype)
        to_attach.add_header('Content-ID', f"<{image.name}>")
        email.attach(to_attach)

    def send_mail(subject, body, sender, recipient, links=[]):
        body_html = Markdown(body, strip_outer_p=True).to_html()

        email_html = render_to_string('email_template.html', context=dict(
            head=subject,
            body=body_html,
            links=links,
        ))

        email = EmailMultiAlternatives(
            subject=html.unescape(str(subject)), body=email_html, from_email=sender, to=recipient
        )
        email.content_subtype = 'html'
        email.send()
