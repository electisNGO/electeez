import os
from pathlib import Path
import sys


BASE_DIR = Path(__file__).resolve().parent.parent

ENVIRONMENT = os.getenv('CI_ENVIRONMENT_NAME', 'localhost')

# SECURITY WARNING: keep the secret key used in production secret!
default = 'notsecretnotsecretnotsecretnotsecretnotsecretnotsecret'
default_crypto = b'frTSrms5O3JGSxPDeam5PTBFrnWbykbdgwUm0UA2rcg='

SECRET_KEY = os.getenv('SECRET_KEY', default)
CRYPTO_KEY = os.getenv('CRYPTO_KEY', default_crypto)

SECURE = SECRET_KEY != default
PROTO = os.getenv('PROTO', 'http')
HOST = os.getenv('HOST', 'localhost:8000')
BASE_URL = '://'.join([PROTO, HOST])

DEFAULT_SITE_ID = 1


DEBUG = ENVIRONMENT == 'localhost'

if SECURE:
    if not HOST.startswith('www'):
        HOST = f'www.{HOST}'
    DEBUG = False
    ALLOWED_HOSTS = [HOST]
else:
    ALLOWED_HOSTS = ['*']

HOSTNAME = HOST.split(':')[0]

if DEBUG:
    os.environ['DJBLOCKCHAIN_MOCK'] = '1'

REGISTRATION_OPEN = True
ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_URL = '/2fa/account/login/'
LOGIN_REDIRECT_URL = '/'
USE_X_FORWARDED_HOST = True

INSTALLED_APPS = [
    'sass_processor',
    'social_django',
    'twilio',
    'electeez_common',
    'electeez_auth',
    'daphne',
    'django.contrib.admin',
    'django.contrib.sites',
    'colorfield',
    'electeez_sites',
    'electeez_consent',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_otp',
    'django_otp.plugins.otp_static',
    'django_otp.plugins.otp_totp',
    'django_session_timeout',
    'two_factor',
    'two_factor.plugins.phonenumber',
    'two_factor.plugins.webauthn',
    'social_2fa',
    'crum',
    'djtezos',
    'djcall',
    'djlang',
    'djelectionguard',
    'djelectionguard.contest',
    'djelectionguard.candidate',
    'djelectionguard.guardian',
    'djelectionguard.voter',
    'djelectionguard_tezos',
    'django_registration',
    'django_extensions',
    'django_jinja',
    'channels',
    'channels_redis',
    'django.forms',
    'defender',
    'ryzom',
    'py2js',
    'ryzom_django',
    'ryzom_django_channels',
    'ryzom_django_mdc',
    'django_celery_beat',
    'captcha'
]

AUTH_USER_MODEL = 'electeez_auth.User'


MIDDLEWARE = [
    'electeez_sites.middleware.HealthCheckMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django_session_timeout.middleware.SessionTimeoutMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'crum.CurrentRequestUserMiddleware',
    'electeez_auth.middleware.FailedLoginMiddleware',
    'electeez_sites.middleware.DynamicSiteDomainMiddleware',
    'csp.middleware.CSPMiddleware',
]

if not DEBUG:
    SESSION_EXPIRE_SECONDS = 3600 * 24
    SESSION_EXPIRE_AFTER_LAST_ACTIVITY = True
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

if DEBUG:
    INSTALLED_APPS += ['debug_toolbar']
    MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']
    DEBUG_TOOLBAR_CONFIG = {
        'DISABLE_PANELS': ("debug_toolbar.panels.redirects.RedirectsPanel",)
    }
    INTERNAL_IPS = ['127.0.0.1']

ROOT_URLCONF = 'electeez_common.urls'

WS_URLPATTERNS = ROOT_URLCONF
WS_HOST = HOSTNAME
WS_PORT = 8081 if HOSTNAME != 'localhost' else os.getenv('WS_PORT', 8000)
SERVER_METHODS = []

ASGI_APPLICATION = 'electeez_common.asgi.application'

REDIS_PROTO = os.getenv('REDIS_PROTO', 'redis')
REDIS_HOST = os.getenv('REDIS_HOST', 'redis')
REDIS_PORT = os.getenv('REDIS_PORT', '6379')
REDIS_USERNAME = os.getenv('REDIS_USERNAME', '')
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', '')

REDIS_SERVER = os.getenv(
    'REDIS_SERVER',
    f'{REDIS_PROTO}://{REDIS_USERNAME}:{REDIS_PASSWORD}@{REDIS_HOST}:{REDIS_PORT}/0',
)

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [REDIS_SERVER],
            'symmetric_encryption_keys': [SECRET_KEY]
        },
    },
}

if SENTRY_DSN := os.getenv('SENTRY_DSN'):
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration
    from sentry_sdk.integrations.celery import CeleryIntegration
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        environment=ENVIRONMENT,
        release=os.getenv('CI_COMMIT_SHA'),
        integrations=[DjangoIntegration(), CeleryIntegration(monitor_beat_tasks=True)],
        traces_sample_rate=0.2,
        attach_stacktrace=True,
        send_default_pii=True
    )

FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.jinja2.Jinja2',
        'DIRS': [
            BASE_DIR / 'templates',
        ],
        'OPTIONS': {
            'environment': 'electeez_common.settings.jinja2',
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR / 'templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

RYZOM_TEMPLATE_BACKEND = {
    "BACKEND": "ryzom_django.template_backend.Ryzom",
    "OPTIONS": {
        "app_dirname": "components",
        "components_module": "ryzom.components.muicss",
        "components_prefix": "Mui",
        # "components_module": "ryzom.components.django",
        # "components_prefix": "Django",

        "context_processors": [
            "django.template.context_processors.debug",
            "django.template.context_processors.request",
            "django.contrib.auth.context_processors.auth",
            "django.contrib.messages.context_processors.messages",
            "django.template.context_processors.i18n",
            'social_django.context_processors.backends',
            'social_django.context_processors.login_redirect',
        ],
        # "autoescape": True,
        # "auto_reload": DEBUG,
        # "translation_engine": "django.utils.translation",
        # "debug": False,
    }
}
TEMPLATES.insert(0, RYZOM_TEMPLATE_BACKEND)  # noqa: F405


def jinja2(**options):
    from django.templatetags.static import static
    from django.urls import reverse
    from jinja2 import Environment
    env = Environment(**options)
    env.globals.update({
        'static': static,
        'url': reverse,
        'len': len,
        'PROTO': os.getenv('PROTO', 'http'),
    })
    return env


STATICFILES_DIRS = [
    BASE_DIR / 'static'
]

DJBLOCKCHAIN = dict(
    TEZOS_CONTRACTS='djelectionguard_tezos/tezos',
    ACCOUNTS=[]
)

ELECTIONCONTRACT_FILE = os.getenv('ELECTIONCONTRACT_FILE', 'election_compiled.json')
TEZOS_USE_GENERIC_ACCOUNT = True

ipfs_home = Path(os.getenv('HOME')) / '.ipfs'
IPFS_ENABLED = 'IPFS_URL' in os.environ or ipfs_home.exists()
IPFS_URL = os.getenv('IPFS_URL', 'localhost:5001')

IDTOKEN_AUTH_ENABLED = os.getenv('IDTOKEN_AUTH_ENABLED', False)
IDTOKEN_ANON_AUTH_ENABLED = os.getenv('IDTOKEN_ANON_AUTH_ENABLED', False)

TEZOS_AUTH_ENABLED = os.getenv('TEZOS_AUTH_ENABLED', False)
TEZOS_RPC_URL = os.getenv('TEZOS_RPC_URL', 'https://rpc.tzkt.io/mainnet/')
TEZOS_API_URL = os.getenv('TEZOS_API_URL', 'https://api.tzkt.io/')

SOCIAL_AUTH_ENABLED = os.getenv('SOCIAL_AUTH_ENABLED', False)

AUTHENTICATION_BACKENDS = [
    'electeez_auth.backends.SiteModelBackend',
]

if SOCIAL_AUTH_ENABLED:
    SOCIAL_AUTH_USER_MODEL = 'electeez_auth.User'
    SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
    SOCIAL_AUTH_JSONFIELD_ENABLED = True

    SOCIAL_AUTH_PIPELINE = [
         'social_core.pipeline.social_auth.social_details',

         # Get the social uid from whichever service we're authing thru. The uid is
         # the unique identifier of the given user in the provider.
         'social_core.pipeline.social_auth.social_uid',

         # Verifies that the current auth process is valid within the current
         # project, this is where emails and domains whitelists are applied (if
         # defined).
         'social_core.pipeline.social_auth.auth_allowed',

         # Checks if the current social-account is already associated in the site.
         'social_core.pipeline.social_auth.social_user',

         # Make up a username for this person, appends a random string at the end if
         # there's any collision.
         'social_core.pipeline.user.get_username',

         # Send a validation email to the user to verify its email address.
         # Disabled by default.
         # 'social_core.pipeline.mail.mail_validation',

         # Associates the current social details with another user account with
         # a similar email address. Disabled by default.
         # 'social_core.pipeline.social_auth.associate_by_email',

         # Create a user account if we haven't found one yet.
         'social_core.pipeline.user.create_user',

         # Create the record that associates the social account with the user.
         'social_core.pipeline.social_auth.associate_user',

         # Populate the extra_data field in the social record with the values
         # specified by settings (and the default ones like access_token, etc).
         'social_core.pipeline.social_auth.load_extra_data',

         # Update the user record with any changed info from the auth service.
         'social_core.pipeline.user.user_details',
         'social_2fa.social_pipeline.two_factor_auth'
    ]

    if SOCIAL_AUTH_GOOGLE_ENABLED := os.getenv(
        'SOCIAL_AUTH_GOOGLE_ENABLED', False
    ):
        AUTHENTICATION_BACKENDS.insert(
            0,
            'social_core.backends.google.GoogleOAuth2',
        )

        SOCIAL_AUTH_GOOGLE_OAUTH2_USE_UNIQUE_USER_ID = True
        SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.getenv('SOCIAL_AUTH_GOOGLE_KEY')
        SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.getenv(
            'SOCIAL_AUTH_GOOGLE_SECRET'
        )

    if SOCIAL_AUTH_FACEBOOK_ENABLED := os.getenv(
        'SOCIAL_AUTH_FACEBOOK_ENABLED', False
    ):
        AUTHENTICATION_BACKENDS.insert(
            0,
            'social_core.backends.facebook.FacebookOAuth2',
        )

        SOCIAL_AUTH_FACEBOOK_KEY = os.getenv('SOCIAL_AUTH_FACEBOOK_KEY')
        SOCIAL_AUTH_FACEBOOK_SECRET = os.getenv('SOCIAL_AUTH_FACEBOOK_SECRET')
        SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
        SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
            'fields': 'id, name, email'
        }

TWILIO_ENABLED = os.getenv('TWILIO_ENABLED', False)
TWILIO_CALLER_ID = os.getenv('TWILIO_CALLER_ID', 'Electis')

if TWILIO_ENABLED:
    TWILIO_ACCOUNT_SID = os.getenv('TWILIO_ACCOUNT_SID', None)
    TWILIO_AUTH_TOKEN = os.getenv('TWILIO_AUTH_TOKEN', None)
    TWILIO_MESSAGING_SERVICE_SID = os.getenv(
        'TWILIO_MESSAGING_SERVICE_SID', None
    )
    TWO_FACTOR_CALL_GATEWAY = 'two_factor.gateways.twilio.gateway.Twilio'
    TWO_FACTOR_SMS_GATEWAY = 'two_factor.gateways.twilio.gateway.Twilio'
else:
    TWO_FACTOR_CALL_GATEWAY = 'two_factor.gateways.fake.Fake'
    TWO_FACTOR_SMS_GATEWAY = 'two_factor.gateways.fake.Fake'
TWO_FACTOR_WEBAUTHN_RP_NAME = 'Electis'

WSGI_APPLICATION = 'electeez_common.wsgi.application'

MEMCACHED_HOST = os.getenv('MEMCACHED_HOST', 'localhost')

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'electeez_auth.validators.SpecialPasswordValidator'
    },
    {
        'NAME': 'electeez_auth.validators.AlphaPasswordValidator'
    },
    {
        'NAME': 'electeez_auth.validators.LowerCasePasswordValidator'
    },
    {
        'NAME': 'electeez_auth.validators.UpperCasePasswordValidator'
    },
]



# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

EMAIL_HOST = os.getenv('EMAIL_HOST', None)
EMAIL_PORT = os.getenv('EMAIL_PORT', None)
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER', None)
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD', None)
EMAIL_USE_TLS = os.getenv('EMAIL_USE_TLS', None)
EMAIL_USE_SSL = os.getenv('EMAIL_USE_SSL', None)

if EMAIL_HOST:
    EMAIL_BACKEND = 'electeez_common.backend.CustomEmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    # EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    # EMAIL_FILE_PATH = '/tmp/app-messages' # change this to a proper location

ADMINS_EMAIL = []

BALLOT_STORE_CLASS = 'SubmittedBallotStore'

SHOW_BASIC_SETTINGS = True
SHOW_CANDIDATES = True
SHOW_VOTERS = True
SHOW_BLOCKCHAIN = True

CUSTOM_SEND_MAIL_MOD = None

EXACT_SCORES = False
CANDIDATE_ORDER_BY = '-score'
REDIRECT_TO_VOTE = None
LINK_TO_CONTEST_LIST = None
SHOW_CANDIDATES_LIST = True
CSE = False
EDU = False
MANAGED_CONTESTS = False
REFERENDUM = False
SHUFFLE_CANDIDATES = True
HAS_SCRUTATOR = False

CELERY_BROKER_URL = REDIS_SERVER
CELERY_RESULT_BACKEND = REDIS_SERVER
CELERY_WORKER_REDIRECT_STDOUTS = False

if 'ADMINS' in os.environ:
    ADMINS = [
        (email.split('@')[0], email)
        for email in os.getenv('ADMINS').split(',')
    ]
    ADMINS_EMAIL = list(os.getenv('ADMINS').split(','))

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'sass_processor.finders.CssFinder',
    'electeez_common.finders.StaticRootFinder',
]

if 'collectstatic' in sys.argv or not DEBUG:
    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.' \
                          'ManifestStaticFilesStorage'

SILENCED_SYSTEM_CHECKS = ["models.W042", "fields.W904", "auth.E003"]
SILENCED_SYSTEM_CHECKS += ['captcha.recaptcha_test_key_error']

DEFENDER_GET_USERNAME_FROM_REQUEST_PATH = 'electeez_auth.views.get_username_from_request'
DEFENDER_LOCKOUT_TEMPLATE = 'defender_template'
DEFENDER_BEHIND_REVERSE_PROXY = True
DEFENDER_REDIS_URL = REDIS_SERVER

HELPDESK_URL = os.getenv('HELPDESK_URL', 'https://helpdesk.electis.app/help/3345365989')

CSP_DEFAULT_SRC = ("'self'", "https://unpkg.com", "https://fonts.googleapis.com", "https://fonts.gstatic.com", "'unsafe-inline'", "'unsafe-eval'", "https://cdn.polyfill.io")
CSP_STYLE_SRC = ("'self'", "https://fonts.googleapis.com", "https://unpkg.com", "'unsafe-inline'", "https://cdn.jsdelivr.net")
CSP_IMG_SRC = ("'self'", "data:", "https://cdn.jsdelivr.net")
CSP_CONNECT_SRC = ("'self'", "wss:")
CSP_CONNECT_SRC += ("ws:",) if DEBUG else ()

CAPTCHA_IMAGE_SIZE = (188, 60)
CAPTCHA_FONT_SIZE = 32
CAPTCHA_LENGTH = 6
CAPTCHA_FOREGROUND_COLOR = '#507886'
CAPTCHA_LETTER_ROTATION = (-35, 35)
CAPTCHA_FLITE_PATH = '/usr/bin/flite'
CAPTCHA_NOISE_FUNCTIONS = (
    'electeez_common.captcha.noise_arcs',
    'electeez_common.captcha.noise_dots',
)


CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": REDIS_SERVER,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}


from celery.schedules import crontab

CELERY_BEAT_SCHEDULE = {
    "start_ceremony": {
        "task": "electeez_common.tasks.start_ceremony",
        "schedule": crontab(minute="*/1"),
    },
    "open_contest": {
        "task": "electeez_common.tasks.open_contest",
        "schedule": crontab(minute="*/1"),
    },
    "close_contest": {
        "task": "electeez_common.tasks.close_contest",
        "schedule": crontab(minute="*/1"),
    },
     "djtezos_sync": {
         "task": "electeez_common.tasks.djtezos_sync",
         "schedule": crontab(minute="*/1"),
     },
     "djtezos_write": {
         "task": "electeez_common.tasks.djtezos_write",
         "schedule": crontab(minute="*/1"),
     },
     "djtezos_balance": {
         "task": "electeez_common.tasks.djtezos_balance",
         "schedule": crontab(minute="*/1"),
     }
}
