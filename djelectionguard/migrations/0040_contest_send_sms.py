# Generated by Django 3.2.12 on 2022-03-31 02:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djelectionguard', '0039_alter_voter_unique_together'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='send_sms',
            field=models.BooleanField(default=False),
        ),
    ]
