# Generated by Django 4.2.11 on 2024-03-14 08:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djelectionguard', '0057_data_migration_contest_start_before_end'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='contest',
            constraint=models.CheckConstraint(check=models.Q(('start__lt', models.F('end'))), name='start_before_end'),
        ),
    ]
