from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import (
    CandidateListView,
    CandidateCreateView,
    CandidateUpdateView,
    CandidateDeleteView,
)


urlpatterns = [
    path(
        '',
        login_required(CandidateListView.as_view()),
        name='contest_candidate_list'
    ),
    path(
        'create/',
        login_required(CandidateCreateView.as_view()),
        name='contest_candidate_create'
    ),
    path(
        '<uuid:cpk>/update/',
        login_required(CandidateUpdateView.as_view()),
        name='contest_candidate_update'
    ),
    path(
        '<uuid:cpk>/delete/',
        login_required(CandidateDeleteView.as_view()),
        name='contest_candidate_delete'
    )
]
