from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import (
    VotersDetailView,
    VotersUpdateView,
    VoterPhoneView,
    VoterVerifyView,
    VoteView,
    VoteSuccessView,
    VotersDownloadView,
    VotersTokenDownloadView,
)


urlpatterns = [
    path(
        '',
        login_required(VotersDetailView.as_view()),
        name='contest_voters_detail'
    ),
    path(
        'download/',
        login_required(VotersDownloadView.as_view()),
        name='contest_voters_download'
    ),
    path('token/',
         login_required(VotersTokenDownloadView.as_view()),
         name='download_voter_token'
    ),
    path(
        'update/',
        login_required(VotersUpdateView.as_view()),
        name='contest_voters_update'
    ),
    path(
        'vote/',
        login_required(VoteView.as_view()),
        name='contest_vote'
    ),
    path(
        '<uuid:vpk>/phone/',
        login_required(VoterPhoneView.as_view()),
        name='voter_phone'
    ),
    path(
        '<uuid:vpk>/verify/',
        login_required(VoterVerifyView.as_view()),
        name='voter_verify'
    ),
    path(
        '<uuid:vpk>/success/',
        login_required(VoteSuccessView.as_view()),
        name='vote_success'
    ),
]
