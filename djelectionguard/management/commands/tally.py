import json

from django.conf import settings
from django.core.management.base import BaseCommand

from redis import StrictRedis as Client

from djelectionguard.models import Contest, decrypt_contest


class Command(BaseCommand):
    help = 'Tally & decrypt an election'

    def handle(self, *args, **options):
        to_decrypt = Contest.objects.filter(decrypting=True)
        client = Client.from_url(settings.REDIS_SERVER)
        for contest in to_decrypt:
            print(f'Decryption of "{contest.name}" started')
            params_str = client.get(f'{contest.pk}-decrypt_params')
            if not params_str:
                print('Decryption params were not found in memory')
                print(
                    'Erasing keys and set contest to the not'
                    ' decrypted state'
                )
                contest.decrypting = False
                for guardian in contest.guardian_set.all():
                    guardian.uploaded = None
                    guardian.save()
                contest.save()
                continue
            params = json.loads(params_str)
            decrypt_contest(*params.values())
            print(f'Decryption of "{contest.name}" finished')
