import json

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from redis import StrictRedis as Client

from djelectionguard.permissions import (
    ContestPermissionMixin,
    contest_key_ceremony_complete,
    contest_is_not_opened_yet,
    contest_contract_created
)
from djelectionguard.models import Contest


class Command(BaseCommand):
    help = 'Open scheduled elections'

    def handle(self, *args, **options):
        print("OPENING ELECTIONS")
        class ContestsToOpen(ContestPermissionMixin):
            permission_chain = [
                contest_key_ceremony_complete,
                contest_is_not_opened_yet,
                contest_contract_created,
            ]

        contests_to_open = ContestsToOpen()

        for contest in contests_to_open.get_queryset():
            if contest.auto_mode:
                if contest.local_start <= contest.local_now:
                    print(f'OPENING CONTEST {contest}')
                    contest.open()
