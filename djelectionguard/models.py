import contextlib
import hashlib
import json
import os
from pathlib import Path
import pickle
import requests
import secrets
import shutil
import subprocess
import textwrap
import uuid

from enum import IntEnum

from datetime import timedelta

from celery.utils.log import get_task_logger

from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone

from djcall.models import Caller
from picklefield.fields import PickledObjectField
from redis import StrictRedis as Client
from timezone_field import TimeZoneField
from twilio.rest import Client as TwClient
from django.utils.translation import gettext_lazy as _

from ryzom_django_channels.pubsub import Publishable, publish
from ryzom_django_channels.views import register
from ryzom.html import A

from electionguard.group import rand_q
from electionguard.tally import CiphertextTally as ct_tally
from electionguard.election_polynomial import LagrangeCoefficientsRecord

from electeez_common.mail import send_mail
from electeez_consent.models import Provider
from electeez_sites.models import Site

from djelectionguard import store as store_module
from djelectionguard import celery_app

logger = get_task_logger(__name__)

def above_0(value):
    if value <= 0:
        raise ValidationError(
            _('Must be above 0, you have choosen:') + f'{value}'
        )


def upload_picture(instance, filename):
    return f'{uuid.uuid4()}.{filename.split(".")[-1]}'


def get_manual_consent_provider():
    manual_providers = Provider.objects.filter(
        provider_class='electeez_consent.manual.Provider',
    )
    if manual_providers.count():
        return manual_providers.values('pk').first()['pk']
    return None


def get_open_email_text():
    return (
        'Hello, Election %(obj)s is open for voting,'
        ' you may use the link below: LINK Happy voting!'
    )


def get_close_email_text():
    return (
        'Hello, Election %(obj)s has been tallied,'
        ' you may use this link below to check the results: LINK'
        ' Thank you for voting on %(obj)s'
    )


def validate_pdf(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf']
    if ext.lower() not in valid_extensions:
        raise ValidationError('Unsupported file extension.')


def validate_sheet(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.xls', '.xlsx', '.csv']
    if ext.lower() not in valid_extensions:
        raise ValidationError('Unsupported file extension.')


class CiphertextTally(models.Model):
    tally = PickledObjectField()

    @classmethod
    def from_tally(cls, tally):
        return cls.objects.create(tally=tally)


class Contest(models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )
    mediator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=255)
    about = models.CharField(
        max_length=2048,
        blank=True,
        null=True
    )
    picture = models.ImageField(
        upload_to=upload_picture,
        blank=True,
        null=True
    )
    type = models.CharField(default='school', max_length=100)
    votes_allowed = models.PositiveIntegerField(
        default=1,
        validators=[above_0],
    )
    quorum = models.IntegerField(
        default=1,
        verbose_name='quorum',
    )
    info = models.DateField(null=True, blank=True)
    start = models.DateTimeField()
    end = models.DateTimeField()
    kc_start = models.DateTimeField(null=True, blank=True)
    timezone = TimeZoneField(
        choices_display='WITH_GMT_OFFSET',
        default='Europe/Paris',
    )

    actual_start = models.DateTimeField(null=True, blank=True, db_index=True)
    actual_end = models.DateTimeField(null=True, blank=True)
    actual_kc_start = models.DateTimeField(null=True, blank=True)

    decrypting = models.BooleanField(default=False)

    joint_public_key = PickledObjectField(null=True, blank=True)
    metadata = PickledObjectField(null=True)
    context = PickledObjectField(null=True)
    device = PickledObjectField(null=True)
    plaintext_tally = PickledObjectField(null=True)
    plaintext_spoiled_ballots = PickledObjectField(null=True)
    ciphertext_tally = models.ForeignKey(
        CiphertextTally,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    coefficient_validation_sets = PickledObjectField(null=True)

    artifacts_sha512 = models.CharField(max_length=255, null=True, blank=True)
    artifacts_ipfs = models.CharField(max_length=255, null=True, blank=True)

    site = models.ForeignKey(
        Site,
        on_delete=models.SET_NULL,
        null=True
    )

    shuffle_candidates = models.BooleanField(default=True)
    exact_scores = models.BooleanField(default=True)
    redirect_to_vote = models.BooleanField(default=False)
    link_to_contest_list = models.BooleanField(default=False)

    consent_provider = models.ForeignKey(
        Provider,
        on_delete=models.SET_NULL,
        default=get_manual_consent_provider,
        null=True
    )

    voters_file = models.FileField(
        upload_to=upload_picture,
        blank=True,
        null=True,
        validators=[validate_sheet]
    )

    open_email_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
    )

    open_email = models.TextField(
        max_length=2048,
        null=True,
        blank=True,
    )

    close_email_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
    )

    close_email = models.TextField(
        max_length=2048,
        null=True,
        blank=True,
    )

    send_open_email = models.BooleanField(default=True)
    send_close_email = models.BooleanField(default=True)
    auto_mode = models.BooleanField(default=False)

    send_sms = models.BooleanField(default=False)

    archived = models.BooleanField(default=False)

    class Meta:
        """ set the default ordering of the model by descending start date
        and add constraint on start/end for start to be lower that end """
        ordering = ['-start']
        constraints = [
            models.CheckConstraint(
                check=models.Q(start__lt=models.F('end')),
                name='start_before_end'
            )
        ]

    @property
    def local_start(self):
        return self.start.replace(tzinfo=self.timezone)

    @property
    def local_end(self):
        return self.end.replace(tzinfo=self.timezone)

    @property
    def local_kc_start(self):
        return self.kc_start.replace(tzinfo=self.timezone)

    def local_datetime(self, value):
        return value.astimezone(self.timezone).replace(tzinfo=self.timezone)

    @property
    def local_now(self):
        return self.local_datetime(timezone.now())

    @property
    def vote_template(self):
        return self.consent_provider.parameters.get(
            'vote_template',
            'contest_vote'
        )

    class PublishStates(IntEnum):
        ELECTION_NOT_DECENTRALIZED = 0,
        ELECTION_CONTRACT_CREATED = 1,
        ELECTION_OPENED = 2,
        ELECTION_CLOSED = 3,
        ELECTION_DECRYPTED = 4,
        ELECTION_PUBLISHED = 5

    @property
    def number_elected(self):
        return self.votes_allowed

    @property
    def number_guardians(self):
        return self.guardian_set.count()

    def get_sequence(self, k):
        client = Client.from_url(settings.REDIS_SERVER)
        sequence = int(client.get(f'{self.pk}.{k}-sequence') or 1)
        client.set(f'{self.pk}.{k}-sequence', sequence + 1)
        return sequence

    @property
    def current_sequence(self):
        return self.get_sequence('contest')

    @property
    def guardian_sequence(self):
        return self.get_sequence('guardian')

    @property
    def candidate_sequence(self):
        return self.get_sequence('candidate')

    @property
    def artifacts_path(self):
        return (
            Path(settings.MEDIA_ROOT)
            / 'artifacts'
            / f'contest-{self.pk}'
        )

    @property
    def artifacts_zip_path(self):
        return Path(f'{str(self.artifacts_path)}.zip')

    @property
    def artifacts_url(self):
        if self.artifacts_ipfs_url:
            return self.artifacts_ipfs_url
        return self.artifacts_local_url

    @property
    def artifacts_local_url(self):
        return ''.join([
            settings.BASE_URL,
            settings.MEDIA_URL,
            'artifacts/',
            f'contest-{self.pk}.zip',
        ])

    @property
    def artifacts_ipfs_url(self):
        if self.artifacts_ipfs:
            return f'https://ipfs.io/ipfs/{self.artifacts_ipfs}'

    @property
    def manifest_url(self):
        return ''.join([
            settings.BASE_URL,
            reverse('contest_manifest', args=[self.pk]),
        ])

    @property
    def manifest_sha512(self):
        from electionguard.serialize import to_raw
        manifest_object = self.get_manifest()
        manifest = to_raw(manifest_object)
        manifest_encoded = manifest.encode('utf-8')
        return hashlib.sha512(manifest_encoded).hexdigest()

    @property
    def store(self):
        store_class = getattr(store_module, settings.BALLOT_STORE_CLASS)
        return store_class(self)

    def voters_hash(self):
        return hashlib.sha512(
            pickle.dumps(
                list(
                    f"{v.pk}-{v.user.pk}-{v.user.email}-{v.token}"
                    for v in self.voter_set.all()
                )
            )
        ).hexdigest()

    def get_fingerprints(self):
        from electionguard.serialize import to_raw
        return {
            'contest.id': str(self.id),
            'contest.name': self.name,
            'contest.description': self.about,
            'contest.joint_public_key': json.loads(to_raw(self.joint_public_key)),
            'contest.voters_hash': self.voters_hash(),
            'contest.manifest_hash': self.manifest_sha512,
            'contest.manifest': json.loads(to_raw(self.get_manifest())),
        }

    def get_tldr_fingerprint(self):
        fingerprints = self.get_fingerprints()
        fingerprints_str = json.dumps(fingerprints)
        return hashlib.sha512(fingerprints_str.encode()).hexdigest()

    def key_ceremony(self):
        from djlang.utils import gettext as _
        from electionguard.key_ceremony import CeremonyDetails
        from electionguard.key_ceremony_mediator import KeyCeremonyMediator

        details = CeremonyDetails(
            self.number_guardians,
            self.quorum,
        )
        mediator = KeyCeremonyMediator('mediator', details)
        guardians_models = self.guardian_set.all().order_by('sequence')

        guardians = [g.get_guardian() for g in guardians_models]

        # round 1
        # ANNONCE
        for guardian in guardians:
            guardian.set_ceremony_details(
                self.number_guardians,
                self.quorum
            )
            mediator.announce(guardian.share_key())

        # Share key
        for guardian in guardians:
            announced_keys = mediator.share_announced()
            for key in announced_keys:
                if guardian.id is not key.owner_id:
                    guardian.save_guardian_key(key)

        # round 2
        # share backups
        for sender in guardians:
            sender.generate_election_partial_key_backups()
            backups = [
                sender.share_election_partial_key_backup(designated.id)
                for designated in guardians
                if designated.id != sender.id
            ]
            mediator.receive_backups(backups)

        # receive backups
        for designated in guardians:
            backups = mediator.share_backups(designated.id)
            for backup in backups:
                designated.save_election_partial_key_backup(backup)

        # round 3
        # Verify backups
        for designated in guardians:
            verifications = []
            for owner in guardians:
                if designated.id is not owner.id:
                    verification = (
                        designated.verify_election_partial_key_backup(owner.id)
                    )
                    verifications.append(verification)
                    owner.save_election_partial_key_verification(verification)
            mediator.receive_backup_verifications(verifications)

        for i, guardian in enumerate(guardians_models):
            guardian.save_guardian(guardians[i])

        joint_key = mediator.publish_joint_key()

        self.joint_public_key = joint_key
        self.save()

        for guardian in self.guardian_set.all():
            send_guardian_mail(
                guardian.id,
                _(
                    'You have been invited to be a guardian of %(election)s',
                    election=self.name
                ),
                _(
                    'The key ceremony has started for %(election)s,'
                    ' please visit LINK to complete the ceremony',
                    election=self.name
                ),
                reverse('contest_guardian_list', args=[self.id])
            )

        for scrutator in self.scrutator_set.all():
            send_scrutator_mail(
                scrutator.id,
                _(
                    'You have been invited to be a scrutator of %(election)s',
                    election=self.name
                ),
                _(
                    'The key ceremony has started for %(election)s,'
                    ' please visit the above link to audit the election',
                    election=self.name
                ),
                reverse('contest_detail', args=[self.id])
            )


    def open(self, send_email=False, email_title=None, email_body=None):
        from djlang.utils import gettext as _
        with transaction.atomic():
            self.prepare()
            self.actual_start = timezone.now()
            self.ciphertext_tally = CiphertextTally.objects.create(
                tally=ct_tally('election-results', self.metadata, self.context)
            )
            store = self.store
            self.save()

        for guardian in self.guardian_set.all():
            guardian.delete_keypair()

        try:
            contract = self.electioncontract
        except ObjectDoesNotExist:
            pass
        else:
            if contract.is_internal:
                contract.open()

        if self.send_open_email:
            self.send_mail(
                self.open_email_title or get_open_email_title(self),
                self.open_email or get_open_email_message(self),
                reverse('contest_list') if self.link_to_contest_list else reverse('contest_vote', args=[self.pk]),
                'open_email_sent'
            )
        send_mail(
            _(
                'Contest %(contest)s is open',
                contest=self.name
            ),
            _('Contest %(contest)s is open since %(actual_start)s',
                contest=self.name,
                actual_start=self.local_datetime(self.actual_start).strftime('%d/%m/%y %Hh%M')
            ),
            settings.DEFAULT_FROM_EMAIL,
            [self.mediator.email]
        )
        register(f"{self.id}:main_section").refresh(contest=self)

    def close(self):
        from djlang.utils import gettext as _
        with transaction.atomic():
            self.actual_end = timezone.now()
            self.save()

        try:
            contract = self.electioncontract
        except ObjectDoesNotExist:
            pass
        else:
            if contract.is_internal:
                contract.close()

        for guardian in self.guardian_set.all():
            send_guardian_mail(
                guardian.id,
                _(
                    '%(election)s is closed',
                    election=self.name
                ),
                _(
                    'To open the ballot box of %(election)s, your key is needed.'
                    ' please visit LINK to open the ballot box.',
                    election=self.name
                ),
                settings.PROTO + '://' +
                self.site.domain +
                reverse('contest_guardian_list', args=[self.id])
            )

        send_mail(
            _(
                'Contest %(contest)s is closed',
                contest=self.name
            ),
            _('Contest %(contest)s is closed since %(actual_end)s',
                contest=self.name,
                actual_end=self.local_datetime(self.actual_end).strftime('%d/%m/%y %Hh%M')
            ),
            settings.DEFAULT_FROM_EMAIL,
            [self.mediator.email]
        )
        register(f"{self.id}:main_section").refresh(contest=self)

    def launch_decryption(
            self,
            send_voters_email,
            email_title,
            email_body,
    ):
        if self.decrypting or self.plaintext_tally:
            return

        decrypt_params = dict(
            contest_id=str(self.pk),
            user_id=str(self.mediator.pk),
            send_voters_email=send_voters_email,
            voters_email_title=email_title,
            voters_email_msg=email_body
        )

        client = Client.from_url(settings.REDIS_SERVER)
        client.set(
            f'{self.pk}-decrypt_params', json.dumps(decrypt_params)
        )

        self.decrypting = True
        self.save()

        try:
            import uwsgi  # noqa
        except ImportError:
            decrypt_contest(
                str(self.pk),
                str(self.mediator.pk),
                send_voters_email,
                email_title,
                email_body
            )

    def decrypt(self):
        decryption_mediator = self.decrypter

        from electionguard.ballot import BallotBoxState
        from electionguard.ballot_box import get_ballots

        submitted_ballots = get_ballots(self.store, BallotBoxState.SPOILED)
        submitted_ballots_list = list(submitted_ballots.values())

        # Decrypt the tally with available guardian keys
        available_guardians = []
        missing_guardians = []

        context = self.context
        tally = self.ciphertext_tally.tally

        for g in self.guardian_set.all().order_by('sequence'):
            guardian = g.get_guardian()
            guardian_key = g.get_election_public_key()
            if g.uploaded:
                print(f'Announcing guardian {g}')
                tally_share = guardian.compute_tally_share(
                    tally, context
                )
                ballot_shares = guardian.compute_ballot_shares(
                    submitted_ballots_list, context
                )
                decryption_mediator.announce(
                    guardian_key, tally_share, ballot_shares
                )
                available_guardians.append(guardian)
            else:
                print(f'adding {g} to missing')
                missing_guardians.append(guardian_key)

        if len(missing_guardians):
            for missing in missing_guardians:
                print('announce missing')
                decryption_mediator.announce_missing(missing)

            for available in available_guardians:
                for missing in missing_guardians:
                    tally_share = available.compute_compensated_tally_share(
                        missing.owner_id,
                        tally,
                        context,
                    )
                    if tally_share is not None:
                        print('receive tally compensation')
                        decryption_mediator.receive_tally_compensation_share(
                            tally_share
                        )

            print('reconstruct shares')
            decryption_mediator.reconstruct_shares_for_tally(tally)

        print('Getting plaintext tally')
        self.plaintext_tally = decryption_mediator.get_plaintext_tally(tally)
        if not self.plaintext_tally:
            raise AttributeError('"self.plaintext_tally" is None')

        # And delete keys from memory
        plaintext_tally_contest = self.plaintext_tally.contests[str(self.pk)]
        print('Setting scores..')
        for candidate in self.candidate_set.all():
            candidate.score = plaintext_tally_contest.selections[
                f'{candidate.pk}-selection'].tally
            print(f'candidate {candidate.name} has {candidate.score} vote(s)')
            candidate.save()

        self.coefficient_validation_sets = LagrangeCoefficientsRecord(
            list(decryption_mediator.get_lagrange_coefficients().values())
        )
        self.decrypting = False
        print('Publishing artifacts..')
        self.publish()

    def publish(self):
        cwd = os.getcwd()

        # provision directory path
        from electionguard.constants import get_constants
        from .export import export
        self.artifacts_path.mkdir(parents=True, exist_ok=True)
        os.chdir(self.artifacts_path)
        export(
            self.description,
            self.context,
            get_constants(),
            [self.device],
            self.store.all(),
            [],
            self.ciphertext_tally.tally.publish(),
            self.plaintext_tally,
            [g.get_guardian().publish() for g in self.guardian_set.all()],
            self.coefficient_validation_sets
        )

        # create the zip file of key to key.zip
        os.chdir(self.artifacts_path / '..')
        name = f'contest-{self.pk}'
        shutil.make_archive(name, 'zip', name)

        sha512 = hashlib.sha512()
        with self.artifacts_zip_path.open('rb') as f:
            while data := f.read(65536):
                sha512.update(data)
        self.artifacts_sha512 = sha512.hexdigest()

        os.chdir(cwd)

        register(f'{self.id}:result_action').refresh(self)

    def publish_ipfs(self):
        try:
            url = f'{settings.IPFS_URL}/api/v0/'
            out = subprocess.check_output(
                ['curl', '-F', f'file=@{self.artifacts_zip_path}', f'{url}add'],
                stderr=subprocess.PIPE,
            )
            result = json.loads(out)
            self.artifacts_ipfs = result['Hash']
            self.save()
            out = subprocess.check_output(
                ['curl', '-X', 'POST', f'{url}pin/add?arg={self.artifacts_ipfs}'],
                stderr=subprocess.PIPE,
            )
        except Exception as e:
            print(e)
            print('Could not upload to IPFS, see error above')

    @property
    def state(self):
        if self.actual_end:
            return 'finished'
        elif self.actual_start:
            return 'started'
        return 'pending'

    @property
    def publish_state(self):
        if self.artifacts_ipfs:
            return self.PublishStates.ELECTION_PUBLISHED
        elif self.plaintext_tally:
            return self.PublishStates.ELECTION_DECRYPTED
        elif self.actual_end:
            return self.PublishStates.ELECTION_CLOSED
        elif self.actual_start:
            return self.PublishStates.ELECTION_OPENED
        elif getattr(self, 'electioncontract', None):
            return self.PublishStates.ELECTION_CONTRACT_CREATED
        else:
            return self.PublishStates.ELECTION_NOT_DECENTRALIZED

    @property
    def variation(self):
        from electionguard.manifest import VoteVariationType
        return (
            VoteVariationType.one_of_m
            if self.votes_allowed == 1
            else VoteVariationType.n_of_m
        )

    def get_absolute_url(self):
        return reverse('contest_detail', args=[self.pk])

    def get_ballot(self, *selections):
        from electionguard.ballot import (
            PlaintextBallot,
            PlaintextBallotContest,
            PlaintextBallotSelection,
        )
        candidates = Candidate.objects.filter(
            pk__in=selections,
            contest_id=self.id
        )
        selections_kwargs = [
            dict(
                sequence_order=candidate.sequence,
                object_id=f'{candidate.pk}-selection',
                vote=1,
                is_placeholder_selection=False,
                extended_data=None
            ) for candidate in candidates
        ]

        ballot = PlaintextBallot(
            object_id=str(uuid.uuid4()),
            style_id=f"{self.pk}-style",
            contests=[
                PlaintextBallotContest(
                    sequence_order=0,
                    object_id=str(self.pk),
                    ballot_selections=[
                        PlaintextBallotSelection(**kw)
                        for kw in selections_kwargs
                    ]
                )
            ]
        )
        return ballot

    @property
    def description(self):
        from electionguard.manifest import Manifest
        return Manifest(
            **self.get_manifest()
        )

    def prepare(self):
        from electionguard.election_builder import ElectionBuilder
        builder = ElectionBuilder(
            number_of_guardians=self.number_guardians,
            quorum=self.quorum,
            manifest=self.description,
        )
        builder.set_public_key(self.joint_public_key.joint_public_key)
        builder.set_commitment_hash(self.joint_public_key.commitment_hash)

        self.metadata, self.context = builder.build()
        from electionguard.encrypt import (
            EncryptionDevice, generate_device_uuid
        )
        self.device = EncryptionDevice(
            generate_device_uuid(),
            12345,
            67890,
            str(self.pk),  # location: str
        )

    @property
    def encrypter(self):
        from electionguard.encrypt import EncryptionMediator
        return EncryptionMediator(
            self.metadata,
            self.context,
            self.device,
        )

    @property
    def decrypter(self):
        from electionguard.decryption_mediator import DecryptionMediator
        return DecryptionMediator(
            'decryption-mediator',
            self.context,
        )

    @property
    def ballot_box(self):
        from electionguard.ballot_box import BallotBox
        return BallotBox(self.metadata, self.context, self.store)

    def get_manifest(self):
        from electionguard.manifest import (
            BallotStyle,
            Candidate,
            ContestDescription,
            ElectionType,
            GeopoliticalUnit,
            ReportingUnitType,
            SelectionDescription,
            Party,
            InternationalizedText,
            Language
        )
        return dict(
            geopolitical_units=[
                GeopoliticalUnit(
                    type=ReportingUnitType.school,
                    name=self.name,
                    object_id=f'{str(self.pk)}-unit',
                )
            ],
            parties=[
                Party(
                    object_id=f'{str(self.pk)}-party',
                    name=InternationalizedText([Language('default')]),
                )
            ],
            candidates=[
                Candidate(
                    object_id=str(candidate.pk),
                    name=InternationalizedText([Language(candidate.name)]),
                )
                for candidate in self.candidate_set.all()
            ],
            contests=[
                ContestDescription(
                    object_id=str(self.pk),
                    sequence_order=0,
                    ballot_selections=[
                        SelectionDescription(
                            object_id=f"{candidate.pk}-selection",
                            sequence_order=candidate.sequence,
                            candidate_id=str(candidate.pk),
                        )
                        for candidate in self.candidate_set.all()
                    ],
                    ballot_title=InternationalizedText([Language(self.name)]),
                    ballot_subtitle=InternationalizedText([Language(self.name)]),
                    vote_variation=self.variation,
                    electoral_district_id=f"{self.pk}-unit",
                    name=self.name,
                    number_elected=self.number_elected,
                    votes_allowed=self.votes_allowed,
                )
            ],
            ballot_styles=[
                BallotStyle(
                    object_id=f"{self.pk}-style",
                    geopolitical_unit_ids=[f"{self.pk}-unit"],
                    party_ids=[f"{self.pk}-party"],
                )
            ],
            name=InternationalizedText([Language(self.name)]),
            start_date=self.start,
            end_date=self.end,
            election_scope_id=f"{self.pk}-style",
            type=ElectionType.primary,
            spec_version="v1.3.0",
        )

    def __str__(self):
        return self.name

    def send_mail(self, title, body, link, field):
        Caller(
            callback='djelectionguard.models.send_contest_mail',
            kwargs=dict(
                contest_id=str(self.pk),
                title=title,
                body=body,
                link=link,
                field=field,
            ),
        ).spool('email')

    def update_voters(self, voters_emails_list=[], auto_only=False):
        # sourcery skip: default-mutable-arg
        from djelectionguard.voter.components import ReactiveStatus
        from djlang.utils import gettext as _

        provider = self.consent_provider

        if not provider.provider.manual:
            register(f'{self.id}:voters_status').refresh(
                self.id,
                str(_(
                    'fetching users from %(provider)s...',
                    provider=provider
                )),
            )
            voters_emails_list = provider.get_active_emails()
        elif auto_only:
            return

        # delete voters who are not anymore in the email list
        self.voter_set.filter(
            casted_at=None
        ).exclude(
            Q(user__email__in=voters_emails_list)
            | Q(user__wallet__in=voters_emails_list)
        ).delete()

        current = [
            *list(self.voter_set.filter(user__wallet=None).values_list(
                'user__email', flat=True
            )),
            *list(self.voter_set.filter(user__email=None).values_list(
                'user__wallet', flat=True
            ))
        ]

        register(f'{self.id}:voters_status').refresh(
            self.id,
            str(_('adding fetched users...')),
        )
        register(f'{self.id}:voters_total').refresh(self.id),
        register(f'{self.id}:voters_added').refresh(
            self.id,
            added=len(current),
            count=len(voters_emails_list)
        )

        # add new voters who have a user
        User = get_user_model()
        site = Site.objects.get_current()
        users = User.objects.filter(
            Q(email__in=voters_emails_list)
            | Q(wallet__in=voters_emails_list),
            siteaccess__site=site
        )
        for user in users:
            print(f'{user} already exists')
            if ((user.email and user.email.lower() in current)
                    or (user.wallet and user.wallet in current)):
                print(f'{user.email} already in voters')
                continue
            Voter.objects.create(
                user=user,
                contest=self
            )
            current.append(user.email or user.wallet)

            register(f'{self.id}:voters_added').refresh(
                self.id,
                added=len(current),
                count=len(voters_emails_list)
            )

        # add new voters who do not have a user
        for addr in voters_emails_list:
            if addr in current:
                print(f'{addr} already has a voter')
                continue
            print(f'{addr} has no user, creating..')
            if '@' in addr:
                user = User.objects.create_user(email=addr, is_active=True)
            else:
                user = User.objects.create_user(wallet=addr, is_active=True)
            Voter.objects.create(
                user=user,
                contest=self
            )
            current.append(addr)

            register(f'{self.id}:voters_added').refresh(
                self.id,
                added=len(current),
                count=len(voters_emails_list)
            )

        register(f'{self.id}:voters_status').refresh(
            self.id,
            str(_('Finalizing...')),
        )


@celery_app.shared_task(name='tally_ballot')
def tally_ballot(contest_id, ballot_id):
    print('Tallying ballot')
    from electionguard.tally import tally_ballot

    contest = Contest.objects.get(id=contest_id)
    submitted_ballot = contest.store.get(ballot_id)

    # be sure that this is thread-safe:
    query = CiphertextTally.objects.select_for_update().filter(id=contest.ciphertext_tally_id)
    with transaction.atomic():
        ciphertext_tally = query.first()
        ciphertext_tally.tally = tally_ballot(submitted_ballot, contest.ciphertext_tally.tally)
        ciphertext_tally.save()


@celery_app.shared_task(name="async_tally")
def async_tally(obj_id, send_voters_email, email_title, email_body):
    logger.info('Start decryption')
    obj = Contest.objects.get(id=obj_id)
    obj.launch_decryption(            
        send_voters_email,
        email_title,
        email_body,)
    logger.info('Finished decryption')

def get_open_email_title(contest):
    from djlang.utils import gettext as _
    if contest and contest.open_email_title:
        return contest.open_email_title
    return _('Election %(obj)s is open for voting', obj=contest or 'NAME')


def get_open_email_message(contest):
    from djlang.utils import gettext as _
    if contest and contest.open_email:
        return contest.open_email
    return _(
        'Hello,'
        ' Election %(obj)s is open for voting, you may use the link below:'
        ' LINK'
        ' Happy voting!',
        obj=contest or 'NAME'
    )


def get_close_email_title(contest):
    from djlang.utils import gettext as _
    if contest and contest.close_email_title:
        return contest.close_email_title
    return _('Election %(obj)s is has been tallied', obj=contest or 'NAME')


def get_close_email_message(contest):
    from djlang.utils import gettext as _
    if contest and contest.close_email:
        return contest.close_email
    return _(
        'Hello,'
        ' Election %(obj)s has been tallied, you may use this link '
        ' below to check the results: LINK'
        ' Thank you for voting on %(obj)s',
        obj=contest or 'NAME'
    )


def send_contest_mail(contest_id, title, body, link, field, **kwargs):
    voters_pks = Voter.objects.filter(
        contest__pk=contest_id
    ).values_list('pk', flat=True)

    for pk in voters_pks:
        if not settings.EDU:
            send_voter_mail.delay(str(pk), title, body, link, field)
        else:
            send_voter_token(str(pk), title, body, field)


def send_guardian_mail(guardian_id, title, body, link):
    from djlang.utils import gettext as _

    guardian = Guardian.objects.select_related('user').get(pk=guardian_id)
    if not guardian.user.email:
        return

    otp_link = guardian.user.otp_new(redirect=link).get_url(guardian.contest.site)
    guardian.user.save()
    send_mail(
        title,
        body,
        settings.DEFAULT_FROM_EMAIL,
        [guardian.user.email],
        [{'href': otp_link, 'text': _('Go to platform')}]
    )


def send_scrutator_mail(scrutator_id, title, body, link):
    scrutator = Scrutator.objects.select_related('user').get(pk=scrutator_id)
    if not scrutator.user.email:
        return

    otp_link = scrutator.user.otp_new(redirect=link).get_url(scrutator.contest.site)
    send_mail(
        title,
        body,
        settings.DEFAULT_FROM_EMAIL,
        [scrutator.user.email],
        [{'href': otp_link, 'text': _('Go to platform')}]
    )


@celery_app.shared_task(name='send_voter_mail')
def send_voter_mail(voter_id, title, body, link, field):
    from djlang.utils import gettext as _

    voter = Voter.objects.select_related('user').get(pk=voter_id)
    if not (voter.user.email or voter.user.contact_email):
        return

    if getattr(voter, field):
        return

    print(f'Mailing user {voter_id}')
    otp_link = voter.user.otp_new(redirect=link).get_url(voter.contest.site)
    voter.user.save()
    send_mail(
        title,
        body,
        settings.DEFAULT_FROM_EMAIL,
        [voter.user.email or voter.user.contact_email],
        [{'href': otp_link, 'text': _('Go to platform')}]
    )

    setattr(voter, field, timezone.now())
    voter.save()


def send_voter_token(voter_id, title, body, field):
    from djlang.utils import gettext as _

    voter = Voter.objects.select_related('user').get(pk=voter_id)
    if not (voter.user.email or voter.user.contact_email):
        return

    if getattr(voter, field):
        return

    print(f'Mailing user {voter_id}')

    id_token = ' '.join(textwrap.fill(voter.user.profile, 4).split('\n'))
    full_body = body.replace('TOKEN', id_token)

    link = f'{settings.PROTO}://{settings.HOST}' + reverse('idtoken_login')

    send_mail(
        title,
        full_body,
        settings.DEFAULT_FROM_EMAIL,
        [voter.user.email or voter.user.contact_email],
        [{'href': link, 'text': _('Go to platform')}]
    )

    setattr(voter, field, timezone.now())
    voter.save()

def decrypt_contest(
        contest_id,
        user_id,
        send_voters_email,
        voters_email_title,
        voters_email_msg
):
    print('START OF DECRYPT_CONTEST')
    from djlang.utils import gettext as _

    contest = None
    med_email_msg = None
    has_error = True
    user = get_user_model().objects.get(id=user_id)
    try:
        contest_query = Contest.objects.select_for_update().filter(
            id=contest_id
        )
        with transaction.atomic():
            contest = contest_query.first()
            contest.decrypt()
            contest.save()

        for guardian in Guardian.objects.filter(contest_id=contest_id):
            print(f'Deleting keys for guardian {guardian}')
            guardian.delete_keypair()

        has_error = False
        med_email_msg = _(
            'The contest %(contest)s has been tallied',
            contest=contest.name,
            allow_unsecure=True
        )

    except Contest.DoesNotExist:
        med_email_msg = _('The contest you wanted to decrypt was not found')
    except Exception as e:
        print(e)
        med_email_msg = _(
            'The decryption raised the exception %(exception)s',
            exception=e
        )
    finally:
        if med_email_msg and user.email:
            print('sending mediator email')
            send_mail(
                _(
                    'Contest %(contest)s decryption',
                    contest=contest.name if contest else _('unknown')
                ),
                med_email_msg,
                settings.DEFAULT_FROM_EMAIL,
                [user.email]
            )

        if send_voters_email and not has_error:
            print('sending voters email')
            contest.send_mail(
                voters_email_title,
                voters_email_msg,
                reverse('contest_detail', args=[contest_id]),
                'close_email_sent'
            )
    print('END OF DECRYPT_CONTEST')



class Candidate(models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )
    contest = models.ForeignKey(
        Contest,
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=255)
    subtext = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    description = models.CharField(
        max_length=300,
        blank=True,
        null=True
    )
    picture = models.ImageField(
        upload_to=upload_picture,
        blank=True,
        null=True
    )
    document = models.FileField(
        upload_to=upload_picture,
        blank=True,
        null=True,
        validators=[validate_pdf]
    )
    score = models.IntegerField(null=True)

    sequence = models.PositiveIntegerField(null=True, blank=True)

    profile = models.CharField(
        verbose_name=_('Tezos profile'),
        max_length=255,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.profile or self.name

    def get_profile(self):
        tzp_api_url = 'https://indexer.tzprofiles.com/v1/graphql/'
        payload = dict(
            query=f'query MyQuery {{ tzprofiles_by_pk(account: "{self.name}") {{ alias }} }}',
            variables=None,
            operationName='MyQuery'
        )
        with contextlib.suppress(ConnectionError, TypeError, KeyError):
            response = requests.post(tzp_api_url, json=payload)
            if response.status_code == 200:
                content = response.json()
                self.profile = content['data']['tzprofiles_by_pk']['alias']

    def save(self, *args, **kwargs):
        self.get_profile()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-score', 'name']
        unique_together = [('name', 'contest')]


class Scrutator(models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )

    contest = models.ForeignKey(
        Contest,
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user)


class Guardian(Publishable, models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )
    contest = models.ForeignKey(
        Contest,
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(auto_now_add=True)
    downloaded = models.DateTimeField(null=True, blank=True)
    verified = models.DateTimeField(null=True, blank=True)
    erased = models.DateTimeField(null=True, blank=True)
    uploaded = models.DateTimeField(null=True, blank=True)
    uploaded_erased = models.DateTimeField(null=True, blank=True)
    sequence = models.PositiveIntegerField(null=True, blank=True)
    key_sha512 = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return str(self.user)

    class Meta:
        ordering = ['created']

    def delete_keypair(self):
        client = Client.from_url(settings.REDIS_SERVER)
        client.delete(str(self.pk))
        if not self.erased:
            self.erased = timezone.now()
        else:
            self.uploaded_erased = timezone.now()
        self.save()

    def upload_keypair(self, content):
        client = Client.from_url(settings.REDIS_SERVER)
        client.set(str(self.pk), content)
        self.uploaded = timezone.now()
        self.save()

    def get_keypair(self):  # sourcery skip: extract-method
        client = Client.from_url(settings.REDIS_SERVER)
        result = client.get(str(self.pk))
        if not result:
            from electionguard.guardian import Guardian
            guardian = Guardian(
                f'guardian-{self.pk}',
                self.sequence,
                self.contest.number_guardians,
                self.contest.quorum,
            )
            result = pickle.dumps(guardian)
            client.set(str(self.pk), result)
            self.key_sha512 = hashlib.sha512(result).hexdigest()
            self.save()
        return result

    def get_guardian(self):
        client = Client.from_url(settings.REDIS_SERVER)
        guardian = pickle.loads(self.get_keypair())
        guardian_public_key = client.get(f'{str(self.pk)}:pk')
        if not guardian_public_key:
            client.set(f'{str(self.pk)}:pk', pickle.dumps(guardian.share_key()))
        return guardian

    def get_election_public_key(self):
        client = Client.from_url(settings.REDIS_SERVER)
        return pickle.loads(client.get(f'{str(self.pk)}:pk'))

    def save_guardian(self, guardian):
        client = Client.from_url(settings.REDIS_SERVER)
        result = pickle.dumps(guardian)
        client.set(str(self.pk), result)
        self.key_sha512 = hashlib.sha512(result).hexdigest()
        self.save()

    @publish
    def guardians(cls, user):
        return cls.objects.filter(
            Q(contest__mediator=user)
            | Q(contest__guardian__user=user)
            | Q(contest__scrutator__user=user)
        ).distinct()


class Voter(Publishable, models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4
    )
    contest = models.ForeignKey(
        Contest,
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    casted_at = models.DateTimeField(null=True, blank=True)
    open_email_sent = models.DateTimeField(null=True, blank=True)
    close_email_sent = models.DateTimeField(null=True, blank=True)
    encryption_seed = models.CharField(max_length=255, null=True, blank=True)

    phone_hash = models.CharField(max_length=255, null=True, blank=True)
    phone_verified = models.DateTimeField(null=True, blank=True)
    token = models.PositiveIntegerField(null=True, blank=True)
    token_expiry = models.DateTimeField(null=True, blank=True)

    @publish
    def voters(cls, user):
        return cls.objects.filter(
            Q(contest__mediator=user)
            | Q(contest__guardian__user=user)
            | Q(contest__scrutator__user=user)
        ).distinct()

    def get_seed(self):
        if not self.encryption_seed:
            self.encryption_seed = str(rand_q())
            self.save()

        return self.encryption_seed

    def generate_token(self):
        self.token = str(secrets.randbelow(900_000) + 100_000)
        self.token_expiry = timezone.now() + timedelta(minutes=3)
        self.save()

    @property
    def token_expired(self):
        return self.token_expiry <= timezone.now()

    def send_verification_token(self, phone_number):
        if not settings.TWILIO_ENABLED:
            return False

        from djlang.utils import gettext as _

        account_sid = settings.TWILIO_ACCOUNT_SID
        auth_token = settings.TWILIO_AUTH_TOKEN

        message_body = _(
            '%(sender)s verification code: %(code)s',
            sender=settings.TWILIO_CALLER_ID,
            code=str(self.token).zfill(4)
        )

        if not settings.DEBUG:
            client = TwClient(account_sid, auth_token)
            message = client.messages.create(
                body=str(message_body),
                from_=settings.TWILIO_CALLER_ID,
                to=phone_number
            )
            return message.sid
        else:
            print(message_body)
            return True


class Store(models.Model):
    contest_id = models.UUIDField()


def get_store(instance, filename):
    return f"{instance.store.contest_id}/{filename}"


class Ballot(models.Model):
    id = models.UUIDField(primary_key=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    ballot = models.FileField(upload_to=get_store)

