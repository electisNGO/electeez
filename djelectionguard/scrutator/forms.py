import codecs
import hashlib

from cryptography.fernet import Fernet

from django import forms
from django.conf import settings
from django.utils import timezone
from django.db.models import Q

from djlang.utils import gettext as _

from electeez_sites.models import Site
from electeez_auth.models import User

from ..models import Scrutator


class ScrutatorCreateForm(forms.Form):
    email = forms.CharField(required=False)

    class Meta:
        fields = ['email']

    def clean(self):
        if self.cleaned_data['email']:
            site = Site.objects.get_current()
            email_or_wallet = self.cleaned_data['email']
            user = User.objects.filter(
                Q(email=email_or_wallet)
                | Q(wallet=email_or_wallet),
                siteaccess__site=site
            ).first()
            if not user:
                if '@' in email_or_wallet:
                    user = User.objects.create_user(email=email_or_wallet)

            if self.instance.contest.scrutator_set.filter(user=user):
                raise forms.ValidationError(
                    dict(email=f'{user}' + _('already added!'))
                )

        return self.cleaned_data

    def save(self):
        contest = self.instance.contest
        if self.cleaned_data['email']:
            email_or_wallet = self.cleaned_data['email']
            user = User.objects.filter(
                Q(email=email_or_wallet)
                | Q(wallet=email_or_wallet)
            ).first()
            self.instance.user = user
            self.instance.save()
