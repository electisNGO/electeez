from django import forms
from django.utils.text import slugify
from phonenumber_field.formfields import PhoneNumberField

from djlang.utils import gettext as _

from .base import ProviderBase
from .models import Consent


class SlugifiedCharField(forms.CharField):
    def clean(self, value):
        value = slugify(value)
        return super().clean(value)


TYPE_MAP = {
    'char': forms.CharField,
    'email': forms.EmailField,
    'phone': PhoneNumberField,
    'int': forms.IntegerField,
    'bool': forms.BooleanField,
    'choice': forms.ChoiceField,
    'slug': SlugifiedCharField
}


class Provider(ProviderBase):
    manual = False
    required_keys = [
        'description', 'fields', 'filters'
    ]

    def get_updates(self, date):
        filters = self.parameters['filters']

        consents = self.provider.consent_set.exclude(
            active=None
        ).filter(
            **{
                'value__' + key: val for key, val in filters.items()
            }
        )
        return True, consents.values_list('value__email', flat=True)

    def update_consent(self, user, value):
        user.consent.get_or_create(value=value)
        user.consent.update(value=value)

    def get_consent_users(self, date):
        return Consent.objects.filter(
            value='CONFIRMED'
        ).values_list(
            'user__email',
            flat=True
        )

    def get_field_attrs(self, field):
        cf = field.copy()
        cf.pop('type')
        fname = cf.pop('name')
        return dict(
            label=_(cf.pop('label', fname.capitalize())),
            help_text=_(field.pop('help_text', '')),
            required=field.pop('required', None) is not None,
            **cf
        )

    def get_fields(self):
        fields = {}

        param_fields = self.parameters.get('fields', {})
        for field in param_fields:
            t = field['type']
            if t in TYPE_MAP:
                fields[field['name']] = TYPE_MAP[t](
                    **{
                        key: value
                        for (key, value) in self.get_field_attrs(field).items()
                        if value
                    }
                )

        return fields

    def get_consent_form_class(self):
        return type(
            'ConsentForm',
            (forms.BaseForm,),
            dict(base_fields=self.get_fields())
        )
