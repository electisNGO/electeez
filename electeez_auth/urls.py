from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.urls import include, path
from django.views.generic.base import RedirectView

from .views import (
    ProfileView,
    ProfileUpdateView,
    OTPEmailSuccess,
    OTPLogin,
    LoginView as OTP2FALogin,
    OTPSend,
    TokenLoginView,
    TokenAnonLoginView,
    PasswordResetView,
    RegistrationView,
    CreatePasswordView,
    request_beacon,
    beacon_login
)


urlpatterns = [
    path('profile/', login_required(ProfileView.as_view()), name='profile'),
    path('profile/update/', login_required(ProfileUpdateView.as_view()), name='profile_edit'),
    path('wallet/', request_beacon, name='request_beacon'),
    path('wallet/login/', beacon_login, name='beacon_login'),
    path('otp/success/', OTPEmailSuccess.as_view(), name='otp_email_success'),
    path('otp/<token>/', OTPLogin.as_view(), name='otp_login'),
    path('otp/2fa/login/', OTP2FALogin.as_view(), name='otp_2fa_login'),
    path('otp/', OTPSend.as_view(), name='otp_send'),
    path('', include('social_django.urls', namespace='social')),
    path('password_reset/', PasswordResetView.as_view(), name='password_reset'),
    path('register/', RegistrationView.as_view(), name='django_registration_register'),
    path('', include('django_registration.backends.activation.urls')),
    path('login/', RedirectView.as_view(pattern_name='two_factor:login', permanent=True)),
    path('', include('django.contrib.auth.urls')),
    path('create_password/<uidb64>/<token>/', CreatePasswordView.as_view(), name='create_password'),
]

if settings.IDTOKEN_AUTH_ENABLED:
    urlpatterns = [
        path('token/', TokenLoginView.as_view(), name='token_login')
    ] + urlpatterns

elif settings.IDTOKEN_ANON_AUTH_ENABLED:
    urlpatterns = [
        path('token/', TokenAnonLoginView.as_view(), name='token_login')
    ] + urlpatterns
