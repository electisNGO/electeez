import logging
import secrets
import requests
import random
import string

from datetime import timedelta

from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager, make_password
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.sessions.models import Session
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.urls import reverse
from django.utils.translation import activate
from django.utils.translation import gettext_lazy as _

from phonenumber_field.modelfields import PhoneNumberField
from djelectionguard.models import Contest

from electeez_sites.models import Site


user_login_logger = logging.getLogger(__name__)


def get_client_ip(request):
    return (
        x_forwarded_for.split(',')[0]
        if (x_forwarded_for := request.META.get('HTTP_X_FORWARDED_FOR'))
        else request.META.get('REMOTE_ADDR')
    )


def log_user_login(sender, user, request, **kwargs):
    if request:
        user_login_logger.info(f"{user} logged IN with IP {get_client_ip(request)}")


def log_user_logout(sender, user, request, **kwargs):
    if request:
        user_login_logger.info(f"{user} logged OUT with IP {get_client_ip(request)}")


user_logged_out.connect(log_user_logout)
user_logged_in.connect(log_user_login)


class UserManager(UserManager):
    def get_queryset(self):
        if settings.SITE_ID != 1:
            site_access_or_admin = (
                Q(is_superuser=True)
                | Q(email='bank@elictis.io')
                | Q(siteaccess__site=Site.objects.get_current()))
            return super().get_queryset().filter(site_access_or_admin).distinct()
        return super().get_queryset()

    def create_user(self, email=None, wallet=None, password=None, **extra_fields):
        site = Site.objects.get_current()
        main_site = Site.objects.get(pk=1)

        settings.SITE_ID = main_site.pk
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        email = self.normalize_email(email)
        if email:
            user = self.filter(email=email).first()
        elif wallet:
            user = self.filter(wallet=wallet).first()
        else:
            user = None

        if not user:
            if email:
                user = self.model(email=email, **extra_fields)
            elif wallet:
                user = self.model(wallet=wallet, **extra_fields)

            if password:
                user.password = make_password(password)
            user.save()

        user.siteaccess_set.get_or_create(site=site)
        settings.SITE_ID = site.pk
        return user

    def bulk_create_users_by_email(self, emails=[], **kwargs):
        site = Site.objects.get_current()
        emails = [self.normalize_email(email) for email in emails]
        existing = User.objects.filter(email__in=emails).values_list('email', flat=True)
        emails = [email for email in emails if email not in existing]
        users = [
            User(email=email, is_active=True)
            for email in emails
        ]
        created_users = self.bulk_create(users)
        site_accesses = [
            SiteAccess(site=site, user=user)
            for user in created_users
        ]
        SiteAccess.objects.bulk_create(site_accesses)

        # Create siteaccesses to users already created before
        for email in existing:
            user = User.objects.get(email=email)
            user.siteaccess_set.get_or_create(site=site)

        return created_users


class User(AbstractUser):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    username = None
    email = models.EmailField(
        verbose_name=_('email address'),
        max_length=255,
        null=True,
        blank=True,
        unique=True
    )
    contact_email = models.EmailField(
        verbose_name=_('email address'),
        max_length=255,
        null=True,
        blank=True,
    )
    wallet = models.CharField(
        verbose_name=_('wallet address'),
        max_length=255,
        null=True,
        blank=True,
        unique=True
    )

    profile = models.CharField(
        verbose_name=_('Tezos profile'),
        max_length=255,
        null=True,
        blank=True,
    )

    phone = PhoneNumberField(blank=True)

    can_send_sms = models.BooleanField(default=False)

    def __str__(self):
        return self.wallet or self.email

    @property
    def display_name(self):
        if self.first_name or self.last_name:
            return ' '.join([self.first_name, self.last_name])
        return self.profile or self.email or self.wallet

    def get_profile(self):
        if self.wallet:
            tzp_api_url = 'https://indexer.tzprofiles.com/v1/graphql/'
            try:
                response = requests.post(tzp_api_url, json=dict(
                    query=f'query MyQuery {{ tzprofiles_by_pk(account: "{self.wallet}") {{ alias }} }}',
                    variables=None,
                    operationName='MyQuery'
                ))
            except Exception:
                return
            if response.status_code == 200:
                content = response.json()
                try:
                    self.profile = content['data']['tzprofiles_by_pk']['alias']
                    self.save()
                except (TypeError, KeyError):
                    pass

    class Meta:
        unique_together = (
            ['email', 'wallet']
        )
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_wallet_xor_email",
                check=(
                    models.Q(
                        wallet__isnull=True,
                        email__isnull=False
                    )
                    | models.Q(
                        wallet__isnull=False,
                        email__isnull=True
                    )
                )
            )
        ]

    objects = UserManager()

    def otp_new(self, redirect=None):
        return self.token_set.create(
            redirect=redirect or reverse('contest_list')
        )

    def beacon_new(self):
        return self.beacon_set.create()

    def save(self, *args, **kwargs):
        if self.email:
            # ensure emails are saved lowercased, for compat with
            # VotersEmailsForm
            self.email = self.email.lower()

            if self.email in settings.ADMINS_EMAIL:
                self.is_superuser = True
                self.is_staff = True

        if self.email == '':
            del self.email
        if self.wallet == '':
            del self.wallet

        return super().save(*args, **kwargs)

    def anonymise(self):
        if self.email:
            self.email = get_random_email()
        elif self.wallet:
            self.wallet = get_random_wallet()
        self.is_active = False
        self.is_staff = False
        self.is_superuser = False
        self.first_name = ''
        self.last_name = ''
        self.phone = ''
        self.profile = 'Anonymous'
        self.set_unusable_password()
        self.save()
        self.totpdevice_set.all().delete()
        self.phonedevice_set.all().delete()
        self.token_set.all().delete()
        self.beacon_set.all().delete()
        self.social_auth.all().delete()

        for s in Session.objects.all():
            if user_id := s.get_decoded().get('_auth_user_id', None):
                if user_id == self.id:
                    s.delete()


class SiteAccess(models.Model):
    site = models.ForeignKey(
        Site,
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE
    )


def get_random_email():
    return (
        get_random_str(8)
        + '@'
        + get_random_str(5)
        + '.'
        + get_random_str(2)
    )


def get_random_wallet():
    chars = string.ascii_letters + string.digits
    return 'tz1' + get_random_str(33, chars)


def get_random_str(size, chars=None):
    if not chars:
        chars = string.ascii_lowercase

    return ''.join(random.sample(chars, size))


def default_token():
    return secrets.token_urlsafe()


def default_expiry():
    return timezone.now() + timedelta(days=30)


def beacon_expiry():
    return timezone.now() + timedelta(minutes=15)


class Token(models.Model):
    user = models.ForeignKey(
        'User',
        on_delete=models.CASCADE,
    )
    token = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=default_token,
    )
    expiry = models.DateTimeField(
        null=True,
        blank=True,
        default=default_expiry,
    )
    redirect = models.CharField(max_length=200, null=True, blank=True)
    used = models.DateTimeField(null=True, blank=True)

    @property
    def url(self):
        return settings.BASE_URL + self.path

    @property
    def path(self):
        return reverse('otp_login', args=[self.token])

    @property
    def expired(self):
        return self.expiry <= timezone.now()

    def get_url(self, site=None):
        if site is None:
            site = Site.objects.get_current()
        return settings.PROTO + '://' + site.domain + self.path


class Beacon(models.Model):
    user = models.ForeignKey(
        'User',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    wallet = models.CharField(
        max_length=255,
        null=True,
        blank=True,
    )

    token = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        default=default_token,
    )
    expiry = models.DateTimeField(
        null=True,
        blank=True,
        default=beacon_expiry,
    )
    used = models.DateTimeField(null=True, blank=True)

    @property
    def expired(self):
        return self.expiry <= timezone.now()

    @property
    def encoded(self):
        return '05' + self.token.encode().hex()

    def renew(self):
        self.token = default_token()
        self.expiry = beacon_expiry()
        self.save()


class IDToken(models.Model):
    email = models.EmailField(
        null=True,
        blank=True
    )

    token = models.CharField(
        max_length=255,
        null=True,
        blank=True,
    )

    from_qr = models.BooleanField(
        null=True,
        blank=True
    )

    used = models.DateTimeField(
        null=True,
        blank=True
    )

    contest = models.ForeignKey(
        Contest,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    class Meta:
        unique_together = (
            ['token', 'contest']
        )
