from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from two_factor.plugins.phonenumber.models import PhoneDevice
from two_factor.plugins.phonenumber.admin import PhoneDeviceAdmin

from electeez_consent.models import ProviderAccess

from .views import send_create_password_email
from .models import User, SiteAccess



admin.site.unregister(Group)
admin.site.unregister(PhoneDevice)


class PhoneDeviceAdmin(PhoneDeviceAdmin):
    list_display = ('user', 'name', 'number')
    search_fields = ('user__email', 'name', 'number')
    ordering = ('user', 'name')


admin.site.register(PhoneDevice, PhoneDeviceAdmin)


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""

    class Meta:
        model = User
        fields = ('email',)

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save()
        user.siteaccess_set.create(site_id=settings.SITE_ID)
        send_create_password_email(user)
        return user

    def save_m2m(self, *a, **kw):
        pass


UserAdmin.ordering = ('date_joined',)
UserAdmin.list_display = ('email', 'first_name', 'last_name', 'wallet', 'is_staff')
UserAdmin.search_fields = ('email', 'wallet', 'first_name', 'last_name')
UserAdmin.fieldsets = (
    (None, {'fields': ('email', 'wallet',)}),
    ('Personal info', {'fields': ('profile', 'first_name', 'last_name')}),
    ('Permissions', {
        'fields': ('is_active', 'is_staff', 'is_superuser', 'can_send_sms')
    }),
    ('Important dates', {'fields': ('last_login', 'date_joined')}),
)
UserAdmin.add_form = UserCreationForm


class ProviderAccessInline(admin.TabularInline):
    model = ProviderAccess


UserAdmin.add_fieldsets = (
    (None, {
        'classes': ('wide',),
        'fields': ('email',),
    }),
)
admin.site.register(User, UserAdmin)


class SiteAccessInline(admin.TabularInline):
    model = SiteAccess


UserAdmin.inlines = [ProviderAccessInline, SiteAccessInline]
