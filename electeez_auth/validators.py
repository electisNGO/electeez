import re

from django.core.exceptions import ValidationError

from djlang.utils import gettext as _


class SpecialPasswordValidator:
    def validate(self, password, user=None):
        if not re.search(r'[^a-zA-Z0-9\s]', password):
            raise ValidationError(
                _("This password must contain at least 1 special character.")
            )

    def get_help_text(self):
        return _(
            "This password must contain at least 1 special character."
        )


class AlphaPasswordValidator:
    def validate(self, password, user=None):
        if not re.search(r'[0-9]', password):
            raise ValidationError(
                _("This password must contain at least 1 numeric character.")
            )

    def get_help_text(self):
        return _(
            "This password must contain at least 1 numeric character."
        )


class LowerCasePasswordValidator:
    def validate(self, password, user=None):
        if not re.search(r'[A-Z]', password):
            raise ValidationError(
                _("This password must contain at least 1 uppercase character.")
            )

    def get_help_text(self):
        return _(
            "This password must contain at least 1 uppercase character."
        )


class UpperCasePasswordValidator:
    def validate(self, password, user=None):
        if not re.search(r'[a-z]', password):
            raise ValidationError(
                _("This password must contain at least 1 lowercase character.")
            )

    def get_help_text(self):
        return _(
            "This password must contain at least 1 lowercase character."
        )
