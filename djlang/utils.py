from django.conf import settings
from django.core.cache import cache
from django.utils.translation import get_language
from django.utils.functional import lazy
from django.utils.html import escape
from django.utils.safestring import SafeString

from django.contrib.sites.models import Site

from .models import Language, Text


USE_DEFAULT_SITE_TEXTS = getattr(settings, 'DJLANG_USE_DEFAULT_SITE_TEXTS', False)
CREATE_NEW_FOUND_TEXTS = getattr(settings, 'DJLANG_CREATE_NEW_FOUND_TEXTS', True)
CREATE_ONLY_DEFAULT = getattr(settings, 'DJLANG_CREATE_ONLY_DEFAULT', True)


def _gettext(key, n=0, **ph):
    '''Usage
    given:
        {
            key: 'FOO',
            val: '%(foo)s is %(bar)s',
            nval: '%(foo)s are %(bar)s'
        },

    so:
    _('FOO', 1, foo='fish', bar='something')
    _('FOO', 3, foo='fishes', bar='something')

    will return:
    -->   'fish is something'
    -->   'fishes are something'

    or, given also:
        {
            key: 'FISH',
            val: 'fish',
            nval: 'fishes'
        }

    so:
    _('FOO', 3, foo=_('FISH', 3), bar='something')

    will return:
    -->   'fishes are something'

    '''
    try:
        current_language = get_language()
        current_site = Site.objects.get_current()

        text = Text.objects.get(
            language__site=current_site,
            language__iso=current_language,
            key=key
        )

    except Text.DoesNotExist:
        if USE_DEFAULT_SITE_TEXTS:
            text = Text.objects.filter(
                language__iso=current_language,
                language__site_id=settings.DEFAULT_SITE_ID,
                key=key
            ).first()
        else:
            text = None

        if not text and CREATE_NEW_FOUND_TEXTS:
            filters = dict()
            if USE_DEFAULT_SITE_TEXTS and CREATE_ONLY_DEFAULT:
                filters['site_id'] = settings.DEFAULT_SITE_ID

            for lang in Language.objects.filter(**filters):
                text_obj, created = lang.text_set.get_or_create(key=key)
                if lang.iso == current_language:
                    text = text_obj

        return text.process(n, **ph) if text else key
    except Exception as e:
        print(
            f'djlang - Exception {e} was raised trying'
            f' to get value for key: {key}'
        )
        return key

    return text.process(n, **ph)


def gettext(*args, **kwargs):
    current_language = get_language()
    current_site = Site.objects.get_current()
    
    cache_key = f'{current_site.domain}:{current_language}:{":".join(args)}:{str(kwargs)}'
    cached = cache.get(cache_key)
    if cached:
        return cached

    text = _gettext(*args, **kwargs)
    if not isinstance(text, SafeString):
        text = escape(text)

    cache.set(cache_key, text, 60 * 60)
    return text


gettext = lazy(gettext, str)
